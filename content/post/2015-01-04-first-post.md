---
title: First post!
date: 2023-03-25
---

I've decided to try and create an anti-work community for Australians.

We are advocating for improving working conditions and pay, reducing the number of hours a person has to work, and ending the requirement of working for survival. 

G'Day!
